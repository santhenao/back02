package com.techu.back02.model;

public class UserModel {

    private String userId;

    public UserModel(){}

    public UserModel(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return this.userId;
    }

}
