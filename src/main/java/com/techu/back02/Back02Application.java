package com.techu.back02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Back02Application {

	public static void main(String[] args) {
		SpringApplication.run(Back02Application.class, args);
	}

}
