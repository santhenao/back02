package com.techu.back02.model;

public class ProductPriceOnly {
    private long id;
    private double precio;

    public ProductPriceOnly() {
    }

    public ProductPriceOnly(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
